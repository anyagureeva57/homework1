list1=[0, 49, 2, 0, 99]
list2=[35, 100, 57, 121]
list3=[41, 85, 50, 79, 130, 40]
print('удвоенные элементы коллекции', list(map(lambda x: x * 2, list1)))
print('произведение по-элементно элементов из трех коллекций', list(map(lambda x, y, z: x * y * z, list1, list2, list3)))
print('длина каждого элемента коллекции',list(map(len, list(map(str, list1)))))

def even(x):
    if x%2==0:
        return True
    else:
        return False
print('только четные элементы коллекции', list(filter(even, list1)))

def even(x):
    if x!=0:
        return True
    else:
        return False
print('только непустые элементы коллекции', list(filter(even, list1)))
print('элементы тройками', list(zip(list1, list2, list3)))
print('элементы двойками, элементы второй коллекции удвоены', list(zip(list1, list(map(lambda x: x * 2, list2)))))